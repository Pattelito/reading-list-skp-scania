To add a suggestion,
create a pull request [here](https://codeberg.org/patched/reading-list-skp-scania)
with the suggestion added to this file,
or modify directly if you have write permissions.

Feel free to add additional data, such as links to ebook/audiobook downloads,
authors, date of publication et.c..

---

# Currently reading

### Lenin for Beginners
- author: Richard Appignanesi
- length: ~170p (comic, little text)
- [download (pdf, en)](https://3lib.net/book/2764843/d85ace)

---

# Possible future reading

### Patriots, Traitors and Empire
- author: Stephan Gowans
- length: ~290p 
- [download (epub, en)](http://libgen.li/item/index.php?md5=6D0F058EBBE237791F3E98F54BD7F8BC)

### The Foundation of Leninism
- author: Joseph Stalin
- length: ~90p

### Conquest of Bread
- author: Peter Kropotkin
- length: ~200p

### Mutual Aid
- author: Peter Kropotkin
- length: ~240p

### Bolsheviks and the Worker's Control
- author: Maurice Brinton
- length: ~90p

### Social Reform or Revolution?
- author: Rosa Luxemburg
- published: 1898 (part i), 1908 (part ii)
- length: ~120p

### Socialism: Utopian and Scientific
- author: Friedrich Engels
- published: 1880
- length: ~90p

### The Wretched of the Earth
- author: Frantz Fanon
- published: 1961
- length: ~250p

### Imperialism: The Highest Stage of Capitalism
- author: Vladimir Lenin
- published: 1917
- length: ~190p

### The Society of the Spectacle
- author: Guy Debord
- published: 1967
- length: ~160p

### The Origin of the Family, Private Property and the State
- author: Friedrich Engels
- published: 1884
- length: ~220p

### Fanged Noumena: Collected Writings 1987-2007
- author: Nick Land
- length: varied (10 to 50 pages per text)
- [download (epub)](https://1lib.sk/book/3640589/97833a)
- [download (pdf)](https://1lib.sk/book/2168155/dd0c90)

### The Advent of Netwar
- length: ~130p
- [download (pdf)](https://prantare.xyz/books/politics/advent-of-netwar.pdf)
- [more info](https://www.rand.org/pubs/monograph_reports/MR789.html)

### Networks and Netwars
- length: ~400p
- [download (epub)](https://prantare.xyz/books/politics/networks-and-netwars.epub)
- [download (pdf)](https://prantare.xyz/books/politics/networks-and-netwars.pdf)
- [more info](https://www.rand.org/pubs/monograph_reports/MR1382.html)

### Revolution Betrayed
- author: Leon Trotsky
- length: ~140p (~300p for the english version, large text)
- download:
    [(sv, pdf)](https://www.marxists.org/archive/trotsky/1936/revbet/revbetray.pdf)
    [(en, pdf)](https://1lib.sk/book/938815/5a3794)

### Permanent Revolution
- author: Leon Trotsky
- length: ~80p (~300p for the english version, large text)
- download:
    [(sv, pdf)](https://marxistarkiv.se/strategi-och-taktik/den-permanenta-revolutionen)
    [(en, pdf)](https://1lib.sk/book/2578734/8e71e4)

### Materialism and Empirio-criticism
- author: Vladimir Lenin
- length: ~400p (very large text)
- download:
    [(en, epub)](https://pranta.re/books/philosophy/materialism-and-empirio-criticism.epub)
    [(en, pdf)](https://pranta.re/books/philosophy/materialism-and-empirio-criticism.pdf)

---

# Done

### Left Wing Communism
- author: Vladimir Lenin
- length: ~100p

### On Contradiction
- author: Mao Zedong
- length: ~35p
- [download](https://1lib.sk/book/5240575/13ea8e)

##### Study companion
- length: ~90p
- [download](https://1lib.sk/book/5679824/88343f)

### The State and Revolution
- Author: Vladimir Lenin
- Length: ~190p
- [download (epub, en)](https://prantare.xyz/books/politics/state-and-revolution.epub)
